#!/bin/bash

# Obtengo el deck
SPIN_DECK=$(microk8s.kubectl get pods --namespace airwave-deploy -l "cluster=spin-deck" -o jsonpath="{.items[0].metadata.name}")

# PortForward al deck
microk8s.kubectl port-forward --address 0.0.0.0 --namespace airwave-deploy ${SPIN_DECK} 9000