# Instalar Spinnaker en microk8s 

El script deploy-spinnaker-to-microk8s.sh descarga e instala spinnaker(https://spinnaker.io) en microk8s. 

Los pasos fueron sacados de https://medium.com/@airwavetechio/installing-spinnaker-on-minikube-using-helm-8ea883d0f9f3

Para esto se vale de microk8s y un chart de helm3.

Estos pasos fueron ejecutados en un Debian 10 (Buster) pero deberían funcionar en cualquier otra distribución similar, con algunas variaciones.

## Microk8s
Microk8s es un cluter de kubernetes simple y sencillo, similar a minikube, pero mas liviano.
Este servicio de Spinnaker utiliza bastante memoria RAM, por lo que se recomienda ejecutarlo con al menos 8Gb.

### Instalando Microk8s


```bash
sudo apt install snap snapd
sudo snap install microk8s --classic
sudo su -
sudo microk8s.enable dns dashboard registry helm3
```
Los comandos ahora cambian un poco, se les agrega el microk8s delante, por ejemplo
```bash
microk8s.kubectl get nodes
microk8s.helm list
```

## Deploy de Spinnaker 

Para el deploy de spinnaker se debe correr el siguiente script
```bash
bash deploy-spinnaker-to-microk8s.sh
```

Luego de unos 20, 30 o 40 minutos, deberia ver todos los pods corriendo

```bash
mboscovich@tesla03:~$ microk8s.kubectl get pods --namespace airwave-deploy
NAME                                READY   STATUS      RESTARTS   AGE
spin-clouddriver-f4d669bc9-wdkht    0/1     Running     2          38h
spin-deck-6b56f5f5c9-8bwqt          1/1     Running     2          38h
spin-echo-545795c8ff-zlwgc          0/1     Running     2          38h
spin-front50-585946d987-7kgd8       0/1     Running     2          38h
spin-gate-7f4cc58b79-shx4q          0/1     Running     2          38h
spin-igor-647b459d7-68rn8           0/1     Running     2          38h
spin-orca-5db5455978-pffgd          0/1     Running     2          38h
spin-rosco-7b988f44ff-ggpmh         0/1     Running     2          38h
spinnaker-install-using-hal-xqbpk   0/1     Completed   0          38h
spinnaker-minio-8655b644c7-mjcfd    1/1     Running     2          38h
spinnaker-redis-master-0            1/1     Running     2          38h
spinnaker-redis-slave-0             1/1     Running     2          38h
spinnaker-redis-slave-1             1/1     Running     2          38h
spinnaker-spinnaker-halyard-0       1/1     Running     2          38h
```
El último paso es hacer visible el deck desde nuestro equipo (Port Forward).
Para esto ejecutamos

```bash
bash connect-to-deck.sh
```

Ahora si puede ingresar al deck de spinnaker en http://localhost:9000

